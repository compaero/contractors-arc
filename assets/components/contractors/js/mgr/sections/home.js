Contractors.page.Home = function (config) {
	config = config || {};
	Ext.applyIf(config, {
		components: [{
			xtype: 'contractors-panel-home', renderTo: 'contractors-panel-home-div'
		}]
	});
	Contractors.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(Contractors.page.Home, MODx.Component);
Ext.reg('contractors-page-home', Contractors.page.Home);