Contractors.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateful: true,
		 stateId: 'contractors-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
		 */
		hideMode: 'offsets',
		items: [{
			html: '<h2>' + _('contractors') + '</h2>',
			cls: '',
			style: {margin: '15px 0'}
		}, {
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: true,
			hideMode: 'offsets',
			items: [{
				title: _('contractors_items'),
				layout: 'anchor',
				items: [{
					html: _('contractors_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'contractors-grid-items',
					cls: 'main-wrapper',
				}]
			}]
		}]
	});
	Contractors.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(Contractors.panel.Home, MODx.Panel);
Ext.reg('contractors-panel-home', Contractors.panel.Home);
