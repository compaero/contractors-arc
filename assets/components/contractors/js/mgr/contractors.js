var Contractors = function (config) {
	config = config || {};
	Contractors.superclass.constructor.call(this, config);
};
Ext.extend(Contractors, Ext.Component, {
	page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('contractors', Contractors);

Contractors = new Contractors();