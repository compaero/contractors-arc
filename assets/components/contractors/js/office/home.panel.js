Contractors.panel.Home = function (config) {
	config = config || {};
	Ext.apply(config, {
		baseCls: 'modx-formpanel',
		layout: 'anchor',
		/*
		 stateful: true,
		 stateId: 'contractors-panel-home',
		 stateEvents: ['tabchange'],
		 getState:function() {return {activeTab:this.items.indexOf(this.getActiveTab())};},
		 */
		hideMode: 'offsets',
		items: [{
			xtype: 'modx-tabs',
			defaults: {border: false, autoHeight: true},
			border: false,
			hideMode: 'offsets',
			items: [{
				title: _('contractors_items'),
				layout: 'anchor',
				items: [{
					html: _('contractors_intro_msg'),
					cls: 'panel-desc',
				}, {
					xtype: 'contractors-grid-items',
					cls: 'main-wrapper',
				}]
			}]
		}]
	});
	Contractors.panel.Home.superclass.constructor.call(this, config);
};
Ext.extend(Contractors.panel.Home, MODx.Panel);
Ext.reg('contractors-panel-home', Contractors.panel.Home);
