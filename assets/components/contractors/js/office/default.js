Ext.onReady(function() {
	Contractors.config.connector_url = OfficeConfig.actionUrl;

	var grid = new Contractors.panel.Home();
	grid.render('office-contractors-wrapper');

	var preloader = document.getElementById('office-preloader');
	if (preloader) {
		preloader.parentNode.removeChild(preloader);
	}
});