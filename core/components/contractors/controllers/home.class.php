<?php

/**
 * The home manager controller for Contractors.
 *
 */
class ContractorsHomeManagerController extends ContractorsMainController {
	/* @var Contractors $Contractors */
	public $Contractors;


	/**
	 * @param array $scriptProperties
	 */
	public function process(array $scriptProperties = array()) {
	}


	/**
	 * @return null|string
	 */
	public function getPageTitle() {
		return $this->modx->lexicon('contractors');
	}


	/**
	 * @return void
	 */
	public function loadCustomCssJs() {
		$this->addCss($this->Contractors->config['cssUrl'] . 'mgr/main.css');
		$this->addCss($this->Contractors->config['cssUrl'] . 'mgr/bootstrap.buttons.css');
		$this->addJavascript($this->Contractors->config['jsUrl'] . 'mgr/misc/utils.js');
		$this->addJavascript($this->Contractors->config['jsUrl'] . 'mgr/widgets/items.grid.js');
		$this->addJavascript($this->Contractors->config['jsUrl'] . 'mgr/widgets/items.windows.js');
		$this->addJavascript($this->Contractors->config['jsUrl'] . 'mgr/widgets/home.panel.js');
		$this->addJavascript($this->Contractors->config['jsUrl'] . 'mgr/sections/home.js');
		$this->addHtml('<script type="text/javascript">
		Ext.onReady(function() {
			MODx.load({ xtype: "contractors-page-home"});
		});
		</script>');
	}


	/**
	 * @return string
	 */
	public function getTemplateFile() {
		return $this->Contractors->config['templatesPath'] . 'home.tpl';
	}
}