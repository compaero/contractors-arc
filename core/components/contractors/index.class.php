<?php

/**
 * Class ContractorsMainController
 */
abstract class ContractorsMainController extends modExtraManagerController {
	/** @var Contractors $Contractors */
	public $Contractors;


	/**
	 * @return void
	 */
	public function initialize() {
		$corePath = $this->modx->getOption('contractors_core_path', null, $this->modx->getOption('core_path') . 'components/contractors/');
		require_once $corePath . 'model/contractors/contractors.class.php';

		$this->Contractors = new Contractors($this->modx);
		//$this->addCss($this->Contractors->config['cssUrl'] . 'mgr/main.css');
		$this->addJavascript($this->Contractors->config['jsUrl'] . 'mgr/contractors.js');
		$this->addHtml('
		<script type="text/javascript">
			Contractors.config = ' . $this->modx->toJSON($this->Contractors->config) . ';
			Contractors.config.connector_url = "' . $this->Contractors->config['connectorUrl'] . '";
		</script>
		');

		parent::initialize();
	}


	/**
	 * @return array
	 */
	public function getLanguageTopics() {
		return array('contractors:default');
	}


	/**
	 * @return bool
	 */
	public function checkPermissions() {
		return true;
	}
}


/**
 * Class IndexManagerController
 */
class IndexManagerController extends ContractorsMainController {

	/**
	 * @return string
	 */
	public static function getDefaultController() {
		return 'home';
	}
}