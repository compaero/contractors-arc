<?php
/** @var array $scriptProperties */
/** @var Contractors $Contractors */
if (!$Contractors = $modx->getService('contractors', 'Contractors', $modx->getOption('contractors_core_path', null, $modx->getOption('core_path') . 'components/contractors/') . 'model/contractors/', $scriptProperties)) {
	return 'Could not load Contractors class!';
}

$pdoTools = $modx->getService('pdoTools');

// Do your snippet code here. This demo grabs 5 items from our custom table.
$tpl = $modx->getOption('tpl', $scriptProperties, 'Item');
$id = $modx->getOption('id', $scriptProperties, '0');
$sortby = $modx->getOption('sortby', $scriptProperties, 'name_short');
$sortdir = $modx->getOption('sortbir', $scriptProperties, 'ASC');
$limit = $modx->getOption('limit', $scriptProperties, 5);
$outputSeparator = $modx->getOption('outputSeparator', $scriptProperties, "\n");
$toPlaceholder = $modx->getOption('toPlaceholder', $scriptProperties, false);

// Build query
$c = $modx->newQuery('Contractor');
$c->sortby($sortby, $sortdir);
$c->limit($limit);
if ($id) {
    $c->where(array('id' => $id));
}
$items = $modx->getCollection('Contractor', $c);

// Iterate through items
$list = array();
/** @var Contractor $item */
foreach ($items as $item) {
    if ($pdoTools) {
        $list[] = $pdoTools->getChunk($tpl, $item->toArray());
    } else {
        $list[] = $modx->getChunk($tpl, $item->toArray());
    }
    $list[] = $item->get('id');
}

// Output
$output = implode($outputSeparator, $list);
if (!empty($toPlaceholder)) {
	// If using a placeholder, output nothing and set output to specified placeholder
	$modx->setPlaceholder($toPlaceholder, $output);

	return '';
}
// By default just return output
return $output;
