<?php
include_once 'setting.inc.php';

$_lang['contractors'] = 'Contractors';
$_lang['contractors_menu_desc'] = 'A sample Extra to develop from.';
$_lang['contractors_intro_msg'] = 'You can select multiple items by holding Shift or Ctrl button.';

$_lang['contractors_items'] = 'Items';
$_lang['contractors_item_id'] = 'Id';
$_lang['contractors_item_name'] = 'Name';
$_lang['contractors_item_description'] = 'Description';
$_lang['contractors_item_active'] = 'Active';

$_lang['contractors_item_create'] = 'Create Item';
$_lang['contractors_item_update'] = 'Update Item';
$_lang['contractors_item_enable'] = 'Enable Item';
$_lang['contractors_items_enable'] = 'Enable Items';
$_lang['contractors_item_disable'] = 'Disable Item';
$_lang['contractors_items_disable'] = 'Disable Items';
$_lang['contractors_item_remove'] = 'Remove Item';
$_lang['contractors_items_remove'] = 'Remove Items';
$_lang['contractors_item_remove_confirm'] = 'Are you sure you want to remove this Item?';
$_lang['contractors_items_remove_confirm'] = 'Are you sure you want to remove this Items?';

$_lang['contractors_item_err_name'] = 'You must specify the name of Item.';
$_lang['contractors_item_err_ae'] = 'An Item already exists with that name.';
$_lang['contractors_item_err_nf'] = 'Item not found.';
$_lang['contractors_item_err_ns'] = 'Item not specified.';
$_lang['contractors_item_err_remove'] = 'An error occurred while trying to remove the Item.';
$_lang['contractors_item_err_save'] = 'An error occurred while trying to save the Item.';

$_lang['contractors_grid_search'] = 'Search';
$_lang['contractors_grid_actions'] = 'Actions';