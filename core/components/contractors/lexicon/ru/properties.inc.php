<?php

$_lang['contractors_prop_limit'] = 'Ограничение вывода Предметов на странице.';
$_lang['contractors_prop_outputSeparator'] = 'Разделитель вывода строк.';
$_lang['contractors_prop_sortBy'] = 'Поле сортировки.';
$_lang['contractors_prop_sortDir'] = 'Направление сортировки.';
$_lang['contractors_prop_tpl'] = 'Чанк оформления каждого ряда Предметов.';
$_lang['contractors_prop_toPlaceholder'] = 'Усли указан этот параметр, то результат будет сохранен в плейсхолдер, вместо прямого вывода на странице.';
