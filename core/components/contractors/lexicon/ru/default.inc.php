<?php
include_once 'setting.inc.php';

$_lang['contractors'] = 'Contractors';
$_lang['contractors_menu_desc'] = 'Пример расширения для разработки.';
$_lang['contractors_intro_msg'] = 'Вы можете выделять сразу несколько предметов при помощи Shift или Ctrl.';

$_lang['contractors_items'] = 'Предметы';
$_lang['contractors_item_id'] = 'Id';
$_lang['contractors_item_name'] = 'Название';
$_lang['contractors_item_description'] = 'Описание';
$_lang['contractors_item_active'] = 'Активно';

$_lang['contractors_item_create'] = 'Создать предмет';
$_lang['contractors_item_update'] = 'Изменить Предмет';
$_lang['contractors_item_enable'] = 'Включить Предмет';
$_lang['contractors_items_enable'] = 'Включить Предметы';
$_lang['contractors_item_disable'] = 'Отключить Предмет';
$_lang['contractors_items_disable'] = 'Отключить Предметы';
$_lang['contractors_item_remove'] = 'Удалить Предмет';
$_lang['contractors_items_remove'] = 'Удалить Предметы';
$_lang['contractors_item_remove_confirm'] = 'Вы уверены, что хотите удалить этот Предмет?';
$_lang['contractors_items_remove_confirm'] = 'Вы уверены, что хотите удалить эти Предметы?';
$_lang['contractors_item_active'] = 'Включено';

$_lang['contractors_item_err_name'] = 'Вы должны указать имя Предмета.';
$_lang['contractors_item_err_ae'] = 'Предмет с таким именем уже существует.';
$_lang['contractors_item_err_nf'] = 'Предмет не найден.';
$_lang['contractors_item_err_ns'] = 'Предмет не указан.';
$_lang['contractors_item_err_remove'] = 'Ошибка при удалении Предмета.';
$_lang['contractors_item_err_save'] = 'Ошибка при сохранении Предмета.';

$_lang['contractors_grid_search'] = 'Поиск';
$_lang['contractors_grid_actions'] = 'Действия';